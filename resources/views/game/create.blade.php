@extends('layout.master')
@section('title')
Halaman Tambah Kategori
@endsection
@section

<form>
    <div class="form-group">
      <label>Nama Game</label>
      <input type="text" class="form-control" name="nama">
    </div>
    <div class="form-group">
      <label>Deskripsi Game</label>
      <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection