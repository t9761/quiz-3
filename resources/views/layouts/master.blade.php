<!DOCTYPE html>
<html lang="en">

<head> 
  <title>QUIZ 3</title>
</head>

<body>
  <p>
    <img src="{{asset('public/img/erd.png')}}"> 
  </p>
  
  <!-- script tambahan sweet alert, bukan dari bawaan sb-admin-2 -->
  <script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

  @stack('scripts')


</body>

</html>
